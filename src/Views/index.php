
<?php if ( isset ($_SESSION['logged_user']) ) : ?>
    Авторизован! <br/>
    Привет, <?php echo $_SESSION['logged_user']->name; ?>!<br/>

    <a href="/exit/logout">Выйти</a>

<?php else : ?>

    <div class="startForm">
    Вы не авторизованы<br/>
    <a href="/user/login">Авторизация</a>
    <a href="/registration/registration">Регистрация</a>
    </div>
<?php endif; ?>
