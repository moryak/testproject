<?php

namespace Controllers;

use RedBeanPHP\RedException\SQL;
use System\View;

/**
 * Class registrationController
 * @package Controllers
 */
class registrationController
{
    /**
     * @var array
     */
    private $errorMessage = [
        'passwordName' => '',
        'errorName' => ''
    ];

    /**
     *
     * @return void
     * @throws SQL
     * @throws \ErrorException
     *
     */
    public function actionSet()
    {
        //берем из $_POST - name,password с помощью trim убираем лишние пробелы
        $name = trim($_POST['name']);
        $password = trim($_POST['password']);
        //для того чтобы записаить в БД что-нибудь нужно работать через переменные ($dataBase)
        $dataBase = \R::dispense('registration');//ищем БД с именем registration

        //если валидация не прошла то выводим сообщения об ошибке, иначе записываем в БД и переходим в login
        if (!$this->validation($password, $name)) {
           View::render('registration',
                [
                    'passwordName' => $this->errorMessage['passwordName'],
                    'errorName' => $this->errorMessage['errorName']
                ]
            );
           return;
        }

        $dataBase->password = $password;
        $dataBase->name = $name;
        \R::store($dataBase);

        redirect('/user/login');
    }

    /**
     * @param string $name
     * @return bool
     */
    //
    private function validateName(string $name) : bool
    {
        if (!$name) {
            $this->errorMessage['errorName'] = 'Пустое поле ввода';

            return false;
        }

        $user = \R::findOne('registration', 'name = ?', [$name]);
        if ($user && $user->name === $name) {
            $this->errorMessage['errorName'] = 'Пользователь с таким логином уже существует!';

            return false;
        }

        return true;
    }

    /**
     * @param string $password
     * @return bool
     */
    private function validatePassword(string $password) : bool
    {
        if (!$password) {
            $this->errorMessage['passwordName'] = 'Пустое поле ввода';

            return false;
        }

        $myRegexp = preg_match("/^(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,}$/", $password);
        if (!$myRegexp) {
            $this->errorMessage['passwordName'] = 'Обязательным являтеся 1 буква верхнего регистра и не менее 8 символов';

            return false;
        }

        return true;
    }

    /**
     * @throws \ErrorException
     * @return string|false
     */
    public function actionRegistration()
    {
        View::render('registration');
    }

    /**
     * @param string $password
     * @param string $name
     * @return bool
     */
    private function validation(string $password, string $name) : bool
    {
        return $this->validatePassword($password) && $this->validateName($name);
    }

}
