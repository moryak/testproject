<?php

namespace Controllers;

use System\View;

class mainController
{
    /**
     * @throws \ErrorException
     */
    public function actionIndex()
    {
        // Рендер главной страницы портала
        View::render('index');
    }

}