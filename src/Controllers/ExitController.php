<?php


namespace Controllers;


class exitController
{
    public function actionLogout()
    {
        unset($_SESSION['logged_user']);
        header('Location: /',true, 301);
    }
}