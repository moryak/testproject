<?php

function redirect($url, $statusCode = 303) //
{
    header('Location: http://localhost:8000' . $url, true, $statusCode);
    die();
}