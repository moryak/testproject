<?php

namespace System;

use Controllers\mainController;


class App
{
    /**
     * @throws \ErrorException
     */
    public static function run()
    {
        session_start();
        // Получаем URL запроса
        $path = $_SERVER['REQUEST_URI'];
        // Разбиваем URL на части ($path)
        $pathParts = explode('/', $path);
        // Получаем имя контроллера
        $controller = $pathParts[1];
        // Получаем имя действия
        $action = $pathParts[2];
        //Проверяем если не существует $controller и $action то по умолчанию они будут view и index
        if (empty($controller) && empty($action)) {
            $controller = 'main';
            $action = 'index';
        }
        // Формируем пространство имен для контроллера
        $controller = 'Controllers\\' . ucfirst($controller) . 'Controller';
        // Формируем наименование действия
        $action = 'action' . ucfirst($action);
        if (!class_exists($controller)) {
            throw new \ErrorException('Controller does not exist');
        }
        // Создаем экземпляр класса контроллера
        $objController = new $controller;
        // Если действия у контроллера не существует, выбрасываем исключение
        if (!method_exists($objController, $action)) {
            throw new \ErrorException('action does not exist');
        }
        // Вызываем действие контроллера
        $objController->$action();
    }
}